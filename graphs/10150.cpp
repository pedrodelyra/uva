#include <bits/stdc++.h>

#define MAX 30000

using namespace std;

map<int, string> dictionary;
map<string, int> word_id;
vector<int> graph[MAX];
int dist[MAX], parents[MAX], VISITED = 1;

bool is_doublet(string a, string b) {
	if(a.length() != b.length()) return false;

	int count = 0;
	for(int i = 0; a[i] && b[i]; ++i)
		if(a[i] != b[i]) ++count;
	
	return count == 1;
}

void bfs(int source, int destination) {
	queue<int> q;
	parents[source] = 0;
	dist[source] = VISITED;
	q.push(source);
	if(dictionary[source].length() == dictionary[destination].length()) {
		while(not q.empty()) {
			auto u = q.front();
			q.pop();
			for(auto& v : graph[u]) {
				if(dist[v] != VISITED) {
					parents[v] = u;
					dist[v] = VISITED;
					if(v == destination) {
						q = queue<int>();
						break;
					} else {
						q.push(v);
					}
				}
			}
		}
	}

	stack<int> sol;
	int u = dist[destination] == VISITED ? destination : 0;
	while(u) {
		sol.push(u);
		u = parents[u];
	}

	if(sol.empty()) {
		printf("No solution.\n");
	} else {
		while(not sol.empty()) {
			int u = sol.top();
			printf("%s\n", dictionary[u].c_str());
			sol.pop();
		}
	}

	++VISITED;
}

int main(void) {
	char word[32];
	int k = 0, tc = 0;
	while(gets(word), *word) {
		if(not word_id[word]) {
			word_id[word] = ++k;
			dictionary[k] = word;
		}
	}

	for(int u = 1; u < k; ++u) {
		for(int v = u + 1; v <= k; ++v) {
			string a = dictionary[u], b = dictionary[v];
			if(is_doublet(a, b)) {
				graph[u].push_back(v);
				graph[v].push_back(u);
			}
		}
	}

	char source[32], destination[32];
	while(scanf("%s %s", source, destination) != EOF) {
		int u = word_id[source], v = word_id[destination];
		if(tc++) printf("\n");
		bfs(u, v);
	}

	return 0;
}
