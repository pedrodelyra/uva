#include <bits/stdc++.h>

using namespace std;

int main(void) {
	int n;
	while(scanf("%d", &n), n) {
		int odds = 0;
		for(int i = 0; i < n; ++i) {
			int x;
			scanf("%d", &x);
			odds += x & 1;
		}

		printf((odds & 1) ? "Not possible\n" : "Possible\n");
	}

	return 0;
}
